## install rails
`sudo apt-get install libsqlite3-dev ruby1.9.1-dev`

`gem install rails`

warning:you can install from taobao mirror

`gem sources -a http://ruby.taobao.org`

`gem sources -r https://rubygems.org/`

`gem install rails`

## you can create your own app

`rails new yourproject`

## this project steps
`cd ruby_omdb`

`bundle install`

`bin/rake db:migrate RAILS_ENV=development`

`rails s -b 0.0.0.0`

## visit url
`http://ip:3000/scraper/scrap?movie=inception`
