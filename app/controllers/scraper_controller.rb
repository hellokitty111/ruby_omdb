require 'httparty'

class ScraperController < ApplicationController
  include HTTParty
  def scrap
    url = "http://omdbapi.com/?t=" + params[:movie]
    @response = HTTParty.get(URI.encode(url))
    @result = JSON.parse(@response.body)
    render json: @result
  end
end
